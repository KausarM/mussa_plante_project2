package server;

import java.io.IOException;

/**
 * @author Etienne Plante
 * @author Kausar Mussa
 */
import java.util.HashSet;

import server.*;

/**
 * This class will run on the server side and is used to connect the server code
 * to the backend business code. This class is where the "protocol" will be
 * defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of
	// ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR
	// SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.

	private ISpellingBeeGame spellingBee;

	public ServerController() throws IOException {
		spellingBee = new SpellingBeeGame(' ', 0, new HashSet<String>());
	}

	/**
	 * Action is the method where the protocol translation takes place. This method
	 * is called every single time that the client sends a request to the server. It
	 * takes as input a String which is the request coming from the client. It then
	 * does some actions on the server (using the ISpellingBeeGame object) and
	 * returns a String representing the message to send to the client
	 * 
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		if (inputLine.equals("getAllLetters")) {
			return spellingBee.getAllLetters();
		} else if (inputLine.equals("getCenterLetter")) {
			return String.valueOf(spellingBee.getCenter());
		} else if (inputLine.split(";")[0].equals("wordCheck")) {
			String word = inputLine.split(";")[1];
			String msg = spellingBee.getMessage(word);
			int points = spellingBee.getPointsForWords(word);

			return msg + ":" + points;
		} else if (inputLine.equals("getScore")) {
			int score = spellingBee.getScore();
			return "" + score;
		} else if (inputLine.equals("getBrackets")) {
			int[] brackets = spellingBee.getBrackets();
			String output = "";
			for (int x = 0; x < brackets.length; x++) {
				output = output + Integer.toString(brackets[x]) + ",";
			}
			return output;
		}

		else {
			return "Error 400";
		}
	}
}