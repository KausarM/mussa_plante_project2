package server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * @author Etienne Plante
 */

public class SimpleSpellingBeeGame implements ISpellingBeeGame {

	private String letters;
	private char center;
	private int score;
	private HashSet<String> wordsFound = new HashSet<String>();
	private static HashSet<String> possibleWords = new HashSet<String>();

	/**
	 * 1st constructor, chooses 7 letters randomly
	 * @param center The center letter
	 * @param score The initial score
	 * @param wordsFound The inital words found
	 * @param possibleWords from a certain language given from a file 
	 * @throws IOException
	 */
	public SimpleSpellingBeeGame (char center, int score, HashSet<String> wordsFound, HashSet<String> possibleWords) throws IOException {
		this.letters = fetchAllLetters();
		this.center = center;
		this.score = score;
		this.wordsFound = wordsFound;
		possibleWords = createWordFromFile("datafiles//english.txt");
	}
	
	/**
	 * Creates a hashset from all the possible words
	 * @param path The path from which to create the word list
	 * @return The list of possible words
	 * @throws IOException
	 */
	public HashSet <String> createWordFromFile(String path) throws IOException{
		Path p = Paths.get(path);
		List<String> lines = Files.readAllLines(p);
		HashSet<String> possibleWords = new HashSet<String>(lines);
		return possibleWords;
		// tested and it works, it will return an array of possible words
	}

	@Override
	/**
	 * Returns the amount of points a user should get for a word
	 */
	public int getPointsForWords(String attempt) {
		boolean bonus = panagram(attempt);
		int size = attempt.length();

		if (size < 4)
			return 0;
		else if (size == 4)
			return 1;
		else {
			if (bonus)
				return size + 7;
			else
				return size;
		}
	}
	
	@Override
	/**
	 * Returns the message that should be returned to the user given a word
	 * @param attempt The word the user has sent in
	 */
	public String getMessage(String attempt) {
		boolean length = lengthValidation(attempt);
		boolean lang = languageValidation(attempt);
		boolean requir = requirementValidation(attempt);
		boolean duplicate = duplicateWord(attempt);

		if (length && lang && requir && duplicate) {
			this.wordsFound.add(attempt);
			this.score += getPointsForWords(attempt);
			return "good";
		} else if (!requir) {
			return "Missing center letter";
		} else if (!lang) {
			return "Word does not exist";
		} else if (!length) {
			return "Not enough letters";
		} else if (!duplicate) {
			return "Word already found";
		} else {
			return "Something went terribly wrong";
		}
	}

	@Override
	/**
	 * Returns if the word is long enough
	 * @param The word
	 */
	public boolean lengthValidation(String attempt) {
		return attempt.length() >= 4;
	}

	@Override
	/**
	 * Returns if the word exists
	 * @param the word
	 */
	public boolean languageValidation(String attempt) {
		return possibleWords.contains(attempt);
	}

	@Override
	/**
	 * Returns if the word contains the center letter
	 * @param the word
	 */
	public boolean requirementValidation(String attempt) {
		for (int i = 0; i < attempt.length(); i++) {
			if (attempt.charAt(i) == this.center) {
				return true;
			}
		}

		return false;
	}

	@Override
	/**
	 * Returns the available letters
	 */
	public String getAllLetters() {
		return this.letters;

	}

	@Override
	/**
	 * Sets and returns the new center letter
	 */
	public char getCenterLetter() {
		// picks a random index and then from that picks a char at that index for the
		// center letter
		Random rand = new Random();
		int upperbound = this.letters.length();
		int random_index = rand.nextInt(upperbound);
		this.center = this.letters.charAt(random_index);

		return this.center;
	}

	@Override
	/**
	 * Returns the current score
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * Returns the brackets
	 */
	public int[] getBrackets() {
		int[] brackets = { 2, 4, 5, 7, 8 };

		return brackets;
	}

	@Override
	/**
	 * Creates the set of possible letters
	 */
	public String fetchAllLetters() {
		return "iamycsn";
	}
	
	/**
	 * Returns the words the user has found
	 */
	public HashSet<String> getWordsFound(){
		return this.wordsFound;
	}

	@Override
	/**
	 * Returns true if the word is a pangram
	 * @param attempt The word
	 */
	public boolean panagram(String attempt) {
		boolean[] needed = new boolean[this.letters.length()];
		List<Character> word = new ArrayList<Character>();

		for (char s : attempt.toCharArray()) {
			word.add(s);
		}
		for (int i = 0; i < this.letters.length(); i++) {
			if (word.contains(this.letters.charAt(i))) {
				needed[i] = true;
			} else {
				return false;
			}
		}
		return true;
	}

	@Override
	/**
	 * Checks that the word is possible given the 7 letters
	 */
	public boolean wordPossible(String input) {
		// a helper method that i will call in getBrackets to check if the word is valid
		// and can be counted in the max count of points
		for(char currentLetter : input.toCharArray()) {
			if(!letters.contains(currentLetter + "")) return false;
		}
		return true;
	}

	@Override
	/**
	 * Checks if the user has already found that word
	 */
	public boolean duplicateWord(String attempt) {
		if (this.wordsFound.contains(attempt)) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	/**
	 * Returns the center letter
	 */
	public char getCenter() {
		return this.center;
	}
}
