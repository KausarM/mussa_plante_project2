package server;

import java.util.HashSet;

/**
 * @author Etienne Plante
 * @author Kausar Mussa
 */

public interface ISpellingBeeGame {

	int getPointsForWords(String attempt);
	/**return the number of points that a given word is worth according to the
	// Spelling Bee rules.*/ 

	boolean panagram(String attempt);
	/**check if word is a panagram*/ 

	String getMessage(String attempt);
	/**return a message based on the reason
	 it is rejected or a positive message (e.g. good or great) if it is a valid
	 word.*/

	String getAllLetters();
	/**return the set of 7 letters (as a String) that the spelling bee object is
	storing*/

	char getCenterLetter();
	/**return the center character. That is, the character that is required to be
	 part of every word.*/
	
	HashSet<String> getWordsFound();
	/**returns words found by user and helps keep track in others helper methods*/

	int getScore();
	/**return the current score of the user.*/

	char getCenter();
	/**
	 * returns center letter
	 */

	int[] getBrackets();
	/**
	 * determine the various point categories. and returns an array of int
	 */

	/**the following 5 methods are additional method and 3 separated in 3 so that if
	someone else wants to modify some specifications about
	what makes a word valid he can do so easily.
	for a word to be valid it had to be of length at least 4, a possible
	word(language), contain the center letter, contain only the 7 letters given to user
	and not be a duplicate or else we give a positive message but no points
 	updated */

	boolean lengthValidation(String attempt);

	boolean languageValidation(String attempt);

	boolean requirementValidation(String attempt);

	boolean duplicateWord(String attempt);
	
	boolean wordPossible(String input);

	String fetchAllLetters();
	/**will pick a random combination of 7 letters to give to user*/
}
