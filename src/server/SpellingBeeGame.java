package server;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * @author Kausar Mussa
 */

public class SpellingBeeGame implements ISpellingBeeGame {

	private String letters;
	private char center;
	private int score;
	private HashSet<String> wordsFound = new HashSet<String>();
	private static HashSet<String> possibleWords = new HashSet<String>();

	/**
	 * 1st constructor, chooses 7 letters randomly
	 * @param center The center letter
	 * @param score The initial score
	 * @param wordsFound The inital words found
	 * @param possibleWords from a certain language given from a file 
	 * @throws IOException
	 */
	public SpellingBeeGame(char center, int score, HashSet<String> wordsFound) throws IOException {
		this.letters = fetchAllLetters();
		this.center = getCenterLetter();
		this.score = score;
		this.wordsFound = wordsFound;
		possibleWords = createWordFromFile("datafiles\\english.txt");
	}

	/**
	 * 2nd constructor, 7letters inputed and not picked randomly
	 * @param center The center letter
	 * @param score The initial score
	 * @param wordsFound The inital words found
	 * @param possibleWords from a certain language given from a file  
	 * @throws IOException
	 */
	public SpellingBeeGame(String letters, char center, int score, HashSet<String> wordsFound) throws IOException {
		this.letters = letters;
		this.center = getCenterLetter();
		this.score = score;
		this.wordsFound = wordsFound;
		possibleWords = createWordFromFile("datafiles\\english.txt");
	}
	/**
	 * Creates a hashset from all the possible words
	 * @param path The path from which to create the word list
	 * @return The list of possible words
	 * @throws IOException
	 */
	public static HashSet<String> createWordFromFile(String path) throws IOException {
		Path p = Paths.get(path);
		List<String> lines = Files.readAllLines(p);
		HashSet<String> possibleWords = new HashSet<String>();
		// to standardize all elements i will make them lowercase before adding them to
		// the HashSet to return
		for (String s : lines) {
			possibleWords.add(s.toLowerCase());
		}
		return possibleWords;
		// tested and it works, it will return an array of possible words
	}

	@Override
	/**
	 * Returns the amount of points a user should get for a word
	 */
	public int getPointsForWords(String attempt) {
		boolean bonus = panagram(attempt);
		int size = attempt.length();
		if (size < 4) {
			return 0;
		}
		if (size == 4) {
			return 1;
		}
		if (bonus == true) {
			return (size + 7);
		} else {
			return size;
		}
	}

	@Override
	/**
	 * Returns true if the word is a pangram
	 * @param attempt The word
	 */
	public boolean panagram(String attempt) {
		boolean[] needed = new boolean[this.letters.length()];
		List<Character> word = new ArrayList<Character>();

		for (char s : attempt.toCharArray()) {
			word.add(s);
		}
		for (int i = 0; i < this.letters.length(); i++) {
			if (word.contains(this.letters.charAt(i))) {
				needed[i] = true;
			} else {
				return false;
			}
		}
		return true;
	}

	@Override
	/**
	 * Returns the message that should be returned to the user given a word
	 * @param attempt The word the user has sent in
	 */
	public String getMessage(String attempt) {
		boolean length = lengthValidation(attempt);
		boolean lang = languageValidation(attempt);
		boolean requir = requirementValidation(attempt);
		boolean duplicate = duplicateWord(attempt);
		boolean possible = wordPossible(attempt);

		if (length && lang && requir && duplicate && possible) {
			this.wordsFound.add(attempt);
			this.score += getPointsForWords(attempt);
			return "good";
		} else if (!requir) {
			return "Missing center letter";
		} else if (!lang) {
			return "Word does not exist";
		} else if (!length) {
			return "Not enough letters";
		} else if (!duplicate) {
			return "Word already found";
		} 
		else if (!possible) {
			return "You used other letters.";
		}
			else {
			return "Something went terribly wrong";
		}
	}

	@Override
	/**
	 * Returns the available letters
	 */
	public String getAllLetters() {
		return this.letters;
	}

	@Override
	/**
	 * Return a random center letter
	 */
	public char getCenterLetter() {
		// picks a random index and then from that picks a char at that index for the
		// center letter
		Random rand = new Random();
		int upperbound = this.letters.length();
		int random_index = rand.nextInt(upperbound);
		return this.letters.charAt(random_index);
	}

	@Override
	/**
	 * Returns the current score
	 */
	public int getScore() {
		return this.score;
	}

	@Override
	/**
	 * returns center letter
	 */
	public char getCenter() {
		return this.center;
	}

	/**
	 * Returns the brackets
	 * Brackets is the [] of the different level of points possible
	 */
	public int[] getBrackets() {
		int[] brackets = new int[5];
		int max = 0;
		for (String i : possibleWords) {
			if ((lengthValidation(i) && requirementValidation(i) && wordPossible(i)) == true) {
				max += getPointsForWords(i);
			}
		}
		brackets[0] = (int) (max * 0.25);
		brackets[1] = (int) (max * 0.50);
		brackets[2] = (int) (max * 0.75);
		brackets[3] = (int) (max * 0.90);
		brackets[4] = (int) (max * 1.00);
		return brackets;
	}

	@Override
	/**
	 * Checks that the word is possible given the 7 letters
	 */
	public boolean wordPossible(String input) {
		// a helper method that i will call in getBrackets to check if the word is valid
		// and can be counted in the max count of points
		for(char currentLetter : input.toCharArray()) {
			if(!this.letters.contains(currentLetter + "")) return false;
		}
		return true;
	}
	
	@Override
	/**
	 * Checks if the word is of a length of at least 4
	 */
	public boolean lengthValidation(String attempt) {
		return attempt.length() >= 4;
	}

	@Override
	/**
	 * Checks if the word is a valid word from the possibleWords
	 */
	public boolean languageValidation(String attempt) {
		boolean isValid = false;
		if (possibleWords.contains(attempt)) {
			isValid = true;
		} else {
			isValid = false;
		}
		return isValid;
	}

	@Override
	/**
	 * Checks if the user uses the center letter
	 */
	public boolean requirementValidation(String attempt) {
		boolean isValid = false;
		for (int i = 0; i < attempt.length(); i++) {
			if (attempt.charAt(i) == this.center) {
				isValid = true;
				return isValid;
			}
		}
		return isValid;
	}

	@Override
	/**
	 * Checks if the user has already found that word
	 */
	public boolean duplicateWord(String attempt) {
		if (this.wordsFound.contains(attempt)) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	// works
	public String fetchAllLetters() {
		// method reads from the file of lettercombination to pick a random combination
		Path p = Paths.get("datafiles//letterCombinations.txt");
		String letters = "";
		try {
			List<String> lines = Files.readAllLines(p);
			Random rand = new Random();
			int upperbound = lines.size();
			int random_index = rand.nextInt(upperbound);
			letters = lines.get(random_index);
		} catch (IOException e) {
			System.out.println("IOException Caught while trying to read file provided in the getAllLetters");
		}
		return letters;
	}

	/**
	 * Returns the words the user has found
	 */
	public HashSet<String> getWordsFound() {
		return this.wordsFound;
	}

}
