package client;

import javafx.scene.Node;

/**
 * @author Etienne Plante
 */
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class GameTab extends Tab {
	Client client;

	HBox buttons;
	TextField input;
	Button submitBtn;
	HBox feedbackFields;
	
	/**
	 * The constructor for the game tab
	 * @param name The name of the tab
	 * @param client The client object
	 */
	public GameTab(String name, Client client) {
		super(name);
		this.client = client;

		this.setContent(createTabContent());
	}
	
	/**
	 * Creates the contents of the tab
	 * @return
	 */
	private VBox createTabContent() {
		VBox content = new VBox();

		buttons = createButtons();
		input = new TextField();
		submitBtn = new Button("submit");
		feedbackFields = createFeedbackFields();

		content.getChildren().add(buttons);
		content.getChildren().add(input);
		content.getChildren().add(submitBtn);
		content.getChildren().add(feedbackFields);

		for (Node child : buttons.getChildren()) {
			Button btn = (Button) child;
			btn.setOnAction(new KeyboardKey(input, btn.getText()));
		}

		submitBtn.setOnAction(new Submit(input, (TextField) feedbackFields.getChildren().get(0),
				(TextField) feedbackFields.getChildren().get(1), client));

		return content;
	}
	
	/**
	 * Creates the letter buttons HBox
	 * @return An HBox containing all letters
	 */
	private HBox createButtons() {
		HBox buttons = new HBox();

		String letters = client.sendAndWaitMessage("getAllLetters");
		String centerLetter = client.sendAndWaitMessage("getCenterLetter");
		for (int i = 0; i < 7; i++) {
			String currentLetter = String.valueOf(letters.charAt(i));
			Button btn = new Button(currentLetter);

			if (currentLetter.equals(centerLetter)) {
				btn.setStyle("-fx-background-color: #77f2ec;");
			}

			buttons.getChildren().add(btn);
		}

		buttons.getChildren().add(new Button("delete"));
		buttons.getChildren().add(new Button("clear"));

		return buttons;
	}

	/**
	 * Returns an HBox containing the score and message field
	 */
	private HBox createFeedbackFields() {
		HBox feedback = new HBox();

		TextField msg = new TextField("Let's get Started");
		TextField score = new TextField("0");
		feedback.getChildren().addAll(msg, score);

		return feedback;
	}

	/**
	 * Returns the current score field
	 */
	public TextField getScoreField() {
		return (TextField) feedbackFields.getChildren().get(1);
	}
}