package client;

import javafx.event.ActionEvent;

/**
 * @author Etienne Plante
 */

import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class KeyboardKey implements EventHandler<ActionEvent> {
	TextField input;
	String character;
	
	/**
	 * Constructor
	 * @param input The input field
	 * @param character The character that the key should type
	 */
	public KeyboardKey(TextField input, String character) {
		this.input = input;
		this.character = character;
	}

	@Override
	/**
	 * Adds the letter to the input field
	 */
	public void handle(ActionEvent arg0) {
		String currentText = input.getText();

		if (character == "clear") {
			input.setText("");
		} else if (character == "delete") {
			input.setText(currentText.substring(0, currentText.length() - 1));
		} else {
			input.setText(currentText + character);
		}
	}
}
