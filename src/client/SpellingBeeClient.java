package client;

import javafx.application.Application;

/**
 * @author Kausar Mussa
 * @author Etienne Plante
 */
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class SpellingBeeClient extends Application {
	private Client client = new Client();
	GameTab gameTab;
	ScoreTab scoreTab;
	
	/**
	 * Start the app
	 */
	public void start(Stage stage) {
		Group root = new Group();

		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);

		root.getChildren().add(createTabPane());

		scoreUpdate().textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String old, String newV) {
				scoreTab.refresh();

			}
		});

		stage.setTitle("Spelling Bee");
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * Creates the tab pane containing the game
	 * @return The tab pane
	 */
	private TabPane createTabPane() {
		TabPane tabPane = new TabPane();
		gameTab = new GameTab("Game", client);
		scoreTab = new ScoreTab("Score", client);
		tabPane.getTabs().addAll(gameTab, scoreTab);

		return tabPane;
	}

	private TextField scoreUpdate() {
		TextField score = gameTab.getScoreField();
		return score;

	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}