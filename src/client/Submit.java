package client;

import javafx.event.ActionEvent;

/**
 * @author Etienne Plante
 */
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class Submit implements EventHandler<ActionEvent> {
	TextField inputField, msgField, score;
	String word;
	Client client;
	
	/**
	 * The constructor
	 * @param inputField The input field containing the guess
	 * @param msgField The message field in which the server replies
	 * @param score The score field
	 * @param client The client object
	 */
	public Submit(TextField inputField, TextField msgField, TextField score, Client client) {
		this.inputField = inputField;
		this.msgField = msgField;
		this.score = score;
		this.client = client;
	}

	@Override
	/**
	 * Sends the word to the server and changes the feedback fields
	 */
	public void handle(ActionEvent arg0) {
		String res = client.sendAndWaitMessage("wordCheck;" + inputField.getText());
		String[] splitRes = res.split(":");

		if (splitRes[0].equals("good")) {
			msgField.setText("Good!");
			score.setText(Integer.parseInt(score.getText()) + Integer.parseInt(splitRes[1]) + "");
		} else {
			msgField.setText(splitRes[0]);
		}

		inputField.setText("");
	}

}
