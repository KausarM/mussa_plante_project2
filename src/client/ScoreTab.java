package client;

import javafx.scene.control.Label;

/**
 * @author Kausar Mussa
 */
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class ScoreTab extends Tab {
	
	/**
	 * Declare the labels as local variable
	 */

	Client client;
	HBox hbox = new HBox();
	Label queen = new Label("");
	Label queenScore = new Label("");
	Label genius = new Label();
	Label geniusScore = new Label();
	Label amaz = new Label();
	Label amazScore = new Label();
	Label good = new Label();
	Label goodScore = new Label();
	Label gett = new Label();
	Label gettScore = new Label();
	Label score = new Label();
	Label userScore = new Label();

	
	/**
	 * constructor takes the name of tab and client object 
	 */
	public ScoreTab(String name, Client client) {
		super(name);
		this.client = client;
		this.setContent(TabContent());
	}
	
	/**
	 * create and return the Hbox content, main layout of the score Tab 
	 */
	private HBox TabContent() {

		String brackets = client.sendAndWaitMessage("getBrackets");
		String[] values = brackets.split(",");
		String scoreVal = client.sendAndWaitMessage("getScore");
		queen.setText("Queen Bee");
		queenScore.setText(values[values.length - 1]);
		queen.setTextFill(Color.GREY);
		queenScore.setTextFill(Color.GREY);

		genius.setText("Genius");
		genius.setTextFill(Color.GREY);
		geniusScore.setText(values[values.length - 2]);
		geniusScore.setTextFill(Color.GREY);

		amaz.setText("Amazing");
		amaz.setTextFill(Color.GREY);
		amazScore.setText(values[values.length - 3]);
		amazScore.setTextFill(Color.GREY);

		good.setText("Good");
		good.setTextFill(Color.GREY);
		goodScore.setText(values[values.length - 4]);
		goodScore.setTextFill(Color.GREY);

		gett.setText("Getting there ");
		gett.setTextFill(Color.GREY);
		gettScore.setText(values[0]);
		gettScore.setTextFill(Color.GREY);

		score.setText("Current Score ");
		userScore.setText(scoreVal);
		userScore.setTextFill(Color.RED);

		VBox positions = new VBox(queen, genius, amaz, good, gett, score);

		VBox numbers = new VBox(queenScore, geniusScore, amazScore, goodScore, gettScore, userScore);

		hbox.getChildren().add(positions);
		hbox.getChildren().add(numbers);

		hbox.setStyle("-fx-background-color: white;");
		hbox.setSpacing(10);
		return hbox;
	}
	
	/**
	 * method that will run everytime there is a change on the scoreField of the user to show user the level he has reached
	 *  */
	
	public void refresh() {
		String brackets = client.sendAndWaitMessage("getBrackets");
		String[] values = brackets.split(",");
		String score = client.sendAndWaitMessage("getScore");
		int newScore = Integer.parseInt(score);
		int[] results = new int[values.length];
		for (int i = 0; i < results.length; i++) {
			results[i] = Integer.parseInt(values[i]);
		}

		if (newScore >= results[0] && newScore < results[1]) {
			gett.setTextFill(Color.BLACK);
			gettScore.setTextFill(Color.BLACK);
		}
		if (newScore >= results[1] && newScore < results[2]) {
			gett.setTextFill(Color.BLACK);
			gettScore.setTextFill(Color.BLACK);
			good.setTextFill(Color.BLACK);
			goodScore.setTextFill(Color.BLACK);
		}

		if (newScore >= results[2] && newScore < results[3]) {
			gett.setTextFill(Color.BLACK);
			gettScore.setTextFill(Color.BLACK);
			good.setTextFill(Color.BLACK);
			goodScore.setTextFill(Color.BLACK);
			amaz.setTextFill(Color.BLACK);
			amazScore.setTextFill(Color.BLACK);
		}

		if (newScore >= results[3] && newScore < results[4]) {
			gett.setTextFill(Color.BLACK);
			gettScore.setTextFill(Color.BLACK);
			good.setTextFill(Color.BLACK);
			goodScore.setTextFill(Color.BLACK);
			amaz.setTextFill(Color.BLACK);
			amazScore.setTextFill(Color.BLACK);
			genius.setTextFill(Color.BLACK);
			geniusScore.setTextFill(Color.BLACK);
		}

		if (newScore == results[4]) {
			gett.setTextFill(Color.BLACK);
			gettScore.setTextFill(Color.BLACK);
			good.setTextFill(Color.BLACK);
			goodScore.setTextFill(Color.BLACK);
			amaz.setTextFill(Color.BLACK);
			amazScore.setTextFill(Color.BLACK);
			genius.setTextFill(Color.BLACK);
			geniusScore.setTextFill(Color.BLACK);
			queenScore.setTextFill(Color.BLACK);
			queen.setTextFill(Color.BLACK);
		}
		userScore.setText(score);

	}

}
